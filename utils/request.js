import {
	baseUrl
} from "@/common/config.js"
//导入本地存储方法
import {
	getStorageToken,
	removeStorageToken
} from "@/utils/storage.js"

// 此vm参数为页面的实例，可以通过它引用vuex中的变量
module.exports = (vm) => {
	// 初始化请求配置
	uni.$u.http.setConfig((config) => {
		/* config 为默认全局配置*/
		config.baseURL = baseUrl ; /* 根域名 */
		return config
	})

	// 请求拦截
	uni.$u.http.interceptors.request.use((config) => { 
		// 可使用async await 做异步操作
		// console.log("请求拦截开始：",config)
		
		// 初始化请求拦截器时，会执行此方法，此时data为undefined，赋予默认{}
		config.data = config.data || {}
		
		// 每次请求都携带 token 		
		const token = getStorageToken();
		config.header.Authorization = token; // 参见后端API文档
		
		// 在发送请求之前做些什么
		uni.showLoading({
			title: '加载中...'
		});
		
		// console.log("请求拦截处理后：",config)		
		return config
	}, config => { // 可使用async await 做异步操作
		return Promise.reject(config)
	})

	// 响应拦截
	uni.$u.http.interceptors.response.use((response) => {
		/* 对响应成功做点什么 可使用async await 做异步操作*/
		// console.log("响应拦截开始：",response)
		uni.hideLoading();
		
		const pages = getCurrentPages();
		const page = pages[pages.length - 1];
		console.log("CurrentPages:",pages)
		console.log("Page:",page)
		//console.log("Page fullPath:",page.__page__.fullPath)
		//pages.forEach(item=>{console.log(item.route)})
		
		const result = response.data; //注意后端返回的数据格式，咨询后端或接口API文档
		if (result.code === 1) { //注意返回的code值的含义，
			return result
		}
		
		// 如果Token错误（用户没登录 或 Token过期）
		if (result.code === 401) {
			uni.showModal({
				title: '要重新登录吗?',
				content: result.msg,
				success: (res) => {
					if (res.confirm) {
						removeStorageToken(); //清除本地保存的Token
						
						// #ifndef MP-WEIXIN
						let redirect = page.__page__.fullPath
						// #endif
						// #ifdef MP-WEIXIN
						let redirect=  page.$page.fullPath 
						// #endif
						
						redirect = encodeURIComponent(redirect);
						let url=`/pages/user/Login?redirect=${redirect}`
						console.log("uni.navigateTo url:",url);
						uni.navigateTo({
							url: url
						});
					} else {
						//路由跳转到首页
						uni.reLaunch({
							url: '/pages/home/Home'
						});
					}
				}
			});
			return Promise.reject(result);
		} 	
		
		console.error("返回错误：",result.msg)
		uni.showModal({
			title: '返回错误',
			content: result.msg,
		});		
		return Promise.reject(response)
	}, (response) => {
		uni.hideLoading();
		// 对响应错误做点什么 （statusCode !== 200）
		console.error("响应错误：",response)
		return Promise.reject(response)
	})
}