import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import {fullUrl} from "@/utils/tools.js"

// 引入全局uView
import uView from '@/uni_modules/uview-ui'

Vue.config.productionTip = false
Vue.prototype.$fullUrl = fullUrl

App.mpType = 'app'

Vue.use(uView)

// #ifdef MP
// 引入uView对小程序分享的mixin封装
const mpShare = require('@/uni_modules/uview-ui/libs/mixin/mpShare.js')
Vue.mixin(mpShare)
// #endif

const app = new Vue({
    ...App
})

// 引入请求封装
require('@/utils/request.js')(app)

app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif