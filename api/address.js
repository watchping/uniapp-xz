const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */

/* 
 ** 前端封装API接口模块 ：收货地址address模块
 */
//1 保存/新增收货地址 
// POST /address/save
// | 参数名	 | 参数说明 | 	备注
// | id | 可选参数，如果有id表示做编辑操作，如果没有id参数表示做新增操作
// | name |  姓名
// |  tel |  电话
// |  province  | 省份
// |  city |  城市
// |  county |  区县
// |  area_code |  地区编码，通过省市区选择获取（必填）
// |  postal_code |  邮政编码
// |  address_detail |  详细地址
// |  is_default |  是否选择默认 0 | 1
export function saveAddress(params) {
	return http.post("/address/save",params);
}

// 2 查询收货地址列表
// GET /address/list
// | 参数名 | 参数说明 | 备注 |
// | 无参数 | 是登录用户的收货地址       
export function getAddressList() {
	return http.get("/address/list");
}

// 3 根据id查询收货地址信息
// GET /address/detail
// | 参数名 | 参数说明   | 备注 |
// | id     | 收货地址id |      |
export function getAddressDetailById(id) {
	let params = {id };
	return http.get("/address/detail",{params});
}

// 4 根据id删除收货地址
// POST /address/delete
// | 参数名 | 参数说明   | 备注 |
// | id     | 收货地址id |
export function deleteAddressById(id) {
	let params = {id };
	return http.post("/address/delete",params);
}

// 5 获取默认的收货地址
// GET /address/default
// | 参数名 | 参数说明                                                                   
// | id     | 可选参数，如果传递了id就获取id对应的收货地址，如果没有传递id就获取默认的收货地址 
export function getDefaultAddress(id) {
	let params = {id };
	return http.get("/address/default",{params});
}
