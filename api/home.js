const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */
/***** 首页(Home) 模块 *******/
//==========================
// 获取首页数据 轮播carouselItems、推荐商品recommendedItems、新品newArrialItems、销售排行topSaleItems
// 访问 GET /index
// 参数  无
// 返回数据 结构:详见接口文档
export function getHomeData(){
	return http.get("/index")
}