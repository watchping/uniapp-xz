const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */

/***** 分类(Category) 模块 *******/
//=================================================
// 1. 获取分类信息接口 
// 参数名	参数说明	备注
// 无参数
export function getCategory() {
	return http.get("/category/list");
}

