const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */

/** 
 * 前端封装API接口模块 ：用户(User) 模块
 */
// 用户登录 
export function login(params) {	
	//params {name, password } 
	return http.post("/user/login",params);
} 

// 查询用户 根据用户id 
export function getDetailById(id) {
	let params = {id}
	return http.get("/user/detail",{params});
}

// 用户注册
export function register(params) {
	return http.post("/user/register",params);
}

// 用户编辑 By 用户ID 
export function update(params) {
	return http.post("/user/update",params);
}

// 接口封装 用户修改密码 By用户Id 看后端提供的接口文档
// params{id,password,password2}
export function updatePwd(params) {	
	return http.post("/user/updatePwd",params);
}

// 上传用户头像
export function uploadAvatar(file) {	
	return http.upload("/upload/avatar", {
		filePath: file.url, // 要上传文件资源的路径。
		name:"avatar",// 文件对应的 key , 开发者在服务器端通过这个 key 可以获取到文件二进制内容
	});
	
}




