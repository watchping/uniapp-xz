const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */

/*
 ** 前端封装API接口模块 ：购物车(Cart) 模块
 */

//===============================
//1.购物车列表
//参数:无(获取当前用户的所有记录)
export function getCartList() {
	return http.get("/cart/list");
}

//==============================
//2.添加购物车
// 参数名         参数说明
// product_id   商品编号
// count 商品数量
export function addCart(params) {	
	return http.post("/cart/add",params);
}

//=============================
//3.删除购物车（可以删除多条记录）
// 参数名         参数说明
// ids  多个编号(流水号)，用","分割"
export function deleteCartItem(ids)
{
	let params = {ids}
	return http.get("/cart/delete",{ params });
}

//===========================
//4.修改购物车条目中的购买数量
// 参数名         参数说明
// id   编号(流水号)
// count 商品数量
export function updateCartCount(params)
{
	return http.get("/cart/updatecount",{ params });	
}

//===============================
//5.修改购物车条目中的是否勾选
// 参数名         参数说明
// id   编号(流水号)
// is_checked 是否选中  0 / 1
export function updateCartChecked(params)
{
	return http.get("/cart/updatechecked",{ params });	
}

//=============================
//6.购物车列表（勾选商品）
//参数:无(获取当前用户的所有记录)
export function getCartListChecked()
{
	return http.get("/cart/listchecked");
}

//========================
//7.清空购物车
// 参数:无
export function deleteCartAll()
{
	return http.get("/cart/deleteAll");
}

//========================
//8.全选或全不选
// 参数名         参数说明
// id   编号(流水号)
// is_checked 是否选中  0 /1 
export function updateCartCheckedAll(is_checked)
{
	let params = { is_checked };
	return http.get("/cart/updatecheckedAll",{ params });
}
