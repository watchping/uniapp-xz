const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */

/***** 商品(Product) 模块 *******/
//==========================
// 获取商品列表(分页,检索）
// 访问 GET /product/page
// 参数名  参数说明   备注
// 页号   pageIndex 非必须
// 页大小 pageSize  非必须
// 搜索  keyWord    非必须
// 分类id  category_id非必须
export function getProductList(params={})
{      
  return http.get("/product/page",{params});
}

//===========================
//根据商品编号获取对应的商品详情
// 参数名  参数说明    备注
// id   商品编号     必须
export function getProductDetailById(id)
{
	let  params = {id};
  return http.get("/product/detail",{params});
}

