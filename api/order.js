const { http } = uni.$u
/**
 * get请求所有参数都在方法的第二个参数中，
 * post请求的第二个参数为请求参数params，而第三个参数才为配置项。
 * 详见 uView2.x官网  https://www.uviewui.com/js/http.html
 */

/* 
** 前端封装API接口模块 ：订单Order模块
*/
//===============================================
// 1 创建订单 POST /order/create
// 参数名	参数说明	备注
// address_id	收货地址id
// remark	备注信息
// productList	购买商品数组,如：
//	[{"product_id":1,"count":5,"price":129},
//   {"product_id":2,"count":23,"price":222}]  这里的product_id是指商品id
export function createOrder(params)
{
  return http.post("/order/create",params);     
}

//===================================================
// 2 查询订单列表
// 参数名	参数说明	备注
// 无参数
export function getOrderList()
{
  return http.get("/order/list");
}

//===================================================
// 3 查询订单详情 
// 参数名	参数说明	备注
// id	订单id
export function getOrderDetailById(id)
{
	let params = {id};
  return http.get("/order/detail",{params});
}

//===================================================
// 4 取消订单 
// 参数名	参数说明	备注
// id	订单id
export function cancleOrderById(id)
{
	let params = {id};
	return http.post("/order/cancel",params);
}

//===================================================
// 5 删除订单 
// 参数名	参数说明	备注
// id	订单id
export function deleteOrderById(id)
{
	let params = {id};
	return http.post("/order/delete",params);
}